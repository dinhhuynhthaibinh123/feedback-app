import { useAppSelector } from "../app/hooks";

const FeedbackStats = () => {
  const { feedbacks } = useAppSelector((state) => state.feedbacks);

  const average =
    feedbacks.reduce((acc, { rating }) => acc + rating, 0) / feedbacks.length;

  return (
    <div className="feedback-stats">
      <h4>{feedbacks.length} Reviews</h4>
      <h4>Average Rating: {average.toFixed(1)}</h4>
    </div>
  );
};

export default FeedbackStats;
