import { FaTimes, FaEdit } from "react-icons/fa";
import Card from "./shared/Card";
import {
  deleteFeedback,
  IFeedback,
  setEdit,
} from "../features/feedback/feedbackSlice";
import { useAppDispatch } from "../app/hooks";
interface FeedbackItemProps {
  item: IFeedback;
}

const FeedbackItem: React.FC<FeedbackItemProps> = ({ item }) => {
  const dispatch = useAppDispatch();

  const handlDeleteFeedback = (id: number) => {
    dispatch(deleteFeedback({ id }));
  };

  const handlEditFeedback = (feedback: IFeedback) => {
    dispatch(setEdit(feedback));
  };

  return (
    <Card>
      <div className="num-display">{item.rating}</div>
      <button className="close">
        <FaTimes color="purple" onClick={() => handlDeleteFeedback(item.id)} />
      </button>
      <button className="edit" onClick={() => handlEditFeedback(item)}>
        <FaEdit color="purple" />
      </button>
      <div className="text-display">{item.text}</div>
    </Card>
  );
};

export default FeedbackItem;
