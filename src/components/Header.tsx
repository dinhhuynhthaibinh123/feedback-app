const Header = () => {
  return (
    <div className="container">
      <header
        style={{
          backgroundColor: "rgba(0,0,0,0.4)",
          color: "#ff6a95",
          borderRadius: "15px",
          marginTop: "30px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <div
          style={{
            textDecoration: "none",
            color: "#ff6a95",
          }}
        >
          <h2>Feedback App TypeScript</h2>
        </div>
      </header>
    </div>
  );
};

export default Header;
