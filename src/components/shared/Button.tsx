interface ButtonProps {
  children: React.ReactNode;
  version?: string;
  type?: "button" | "submit" | "reset" | undefined;
  isDisabled: boolean;
}

const Button: React.FC<ButtonProps> = ({
  children,
  version,
  type,
  isDisabled,
}) => {
  return (
    <button
      type={type} 
      disabled={isDisabled}
      className={`btn btn-${version ? "" : "primary"}`}
    >
      {children}
    </button>
  );
};

export default Button;
