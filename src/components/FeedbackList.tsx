import FeedbackItem from "./FeedbackItem";
import { useAppSelector } from "../app/hooks";

const FeedbackList: React.FC = () => {
  const { feedbacks } = useAppSelector((state) => state.feedbacks);

  return (
    <div className="feedback-list">
      {feedbacks && feedbacks.map((feedback) => (
        <FeedbackItem item={feedback} key={feedback.id} />
      ))}
    </div>
  );
};

export default FeedbackList;
