import React from "react";

interface RatingProps {
  setRating: (select: number) => void;
  selected: number;
}
const RatingSelect: React.FC<RatingProps> = ({ setRating, selected = 5 }) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRating(+e.target.value);
  };

  return (
    <ul className="rating">
      {Array.from({ length: 10 }, (_, index) => {
        return (
          <li key={`rating-${index + 1}`}>
            <input
              type="radio"
              id={`num${index + 1}`}
              name="rating"
              value={index + 1}
              onChange={handleChange}
              checked={selected === index + 1}
            />
            <label htmlFor={`num${index + 1}`}>{index + 1}</label>
          </li>
        );
      })}
    </ul>
  );
};

export default RatingSelect;
