import React, { useState, useEffect } from "react";
import Card from "./shared/Card";
import Button from "./shared/Button";
import RatingSelect from "./RatingSelect";
import {
  addFeedback,
  updateFeedback,
} from "../features/feedback/feedbackSlice";
import { useAppDispatch } from "../app/hooks";
import { useAppSelector } from "../app/hooks";

const FeedbackForm = () => {
  const [text, setText] = useState("");
  const [rating, setRating] = useState(10);
  const [btnDisabled, setBtnDisabled] = useState(true);
  const [message, setMessage] = useState<string | null>("");
  const dispatch = useAppDispatch();
  const { feedbackEdit } = useAppSelector((state) => state.feedbacks);

  useEffect(() => {
    if (feedbackEdit.edit === true) {
      if (feedbackEdit.item) {
        setBtnDisabled(false);
        setText(feedbackEdit.item.text);
        setRating(feedbackEdit.item.rating);
      }
    }
  }, [feedbackEdit]);

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    if (value === "") {
      setMessage("Please fill in review form");
    } else if (value.trim().length < 10) {
      setMessage("Text must be at least 10 characters");
      setBtnDisabled(true);
    } else {
      setMessage("");
      setBtnDisabled(false);
    }
    setText(value);
  };

  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (text.trim().length > 10) {
      const id = Math.floor(Math.random() * 1000);
      const newFeedback = {
        id,
        text,
        rating,
      };

      if (feedbackEdit.edit === true) {
        if (feedbackEdit.item) {
          dispatch(
            updateFeedback({ ...newFeedback, id: feedbackEdit.item?.id })
          );
        }
      } else {
        dispatch(addFeedback(newFeedback));
      }

      setBtnDisabled(true);
      setRating(10);
      setText("");
    }
  };

  return (
    <Card>
      <form onSubmit={onSubmit}>
        <h2>How would you rate your service with us?</h2>
        <RatingSelect selected={rating} setRating={setRating} />
        <div className="input-group">
          <input
            type="text"
            placeholder="Write a review"
            value={text}
            onChange={handleTextChange}
          />
          <Button type="submit" isDisabled={btnDisabled}>
            Send
          </Button>
        </div>
        {message && <div className="message">{message}</div>}
      </form>
    </Card>
  );
};

export default FeedbackForm;
