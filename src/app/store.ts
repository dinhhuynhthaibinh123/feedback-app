import { configureStore } from "@reduxjs/toolkit";
import feedbackReducer from "../features/feedback/feedbackSlice";
const store = configureStore({
  reducer: {
    feedbacks: feedbackReducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
