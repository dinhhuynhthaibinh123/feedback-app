import Card from "../components/shared/Card";
import { Link } from "react-router-dom";
const ThaiBinhAbout = () => {
  return (
    <Card>
      <div className="about">
        <h1>About Me</h1>
        <h3>Dinh Huynh Thai Binh</h3>
        <p>This is React Redux App With TypeScript</p>
        <p>Version: 2.3</p>

        <p>
          <Link to="/">Back To Home</Link>
        </p>
      </div>
    </Card>
  );
};

export default ThaiBinhAbout;
