import FeedbackList from "./components/FeedbackList";
import FeedbackForm from "./components/FeedbackForm";
import { Provider } from "react-redux";
import store from "./app/store";
import Header from "./components/Header";
import FeedbackStats from "./components/FeedbackStats";
import AboutIconsLink from "./components/AboutIconsLink";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import ThaiBinhAbout from "./pages/ThaiBinhAbout";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Header />
        <div className="container">
          <Routes>
            <Route
              path="/"
              element={
                <>
                  <FeedbackForm />
                  <FeedbackStats />
                  <FeedbackList />
                </>
              }
            />
            <Route path="about" element={<ThaiBinhAbout />} />
          </Routes>
          <AboutIconsLink />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
