import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import FeedbackData from "../../data/FeedbackData";
export interface IFeedback {
  id: number ;
  rating: number;
  text: string;
}
interface IFeedbackState {
  feedbacks: IFeedback[];
  feedbackEdit: {
    item: IFeedback | null;
    edit: boolean;
  };
}
const initialState: IFeedbackState = {
  feedbacks: FeedbackData,
  feedbackEdit: {
    item: null,
    edit: false,
  },
};

const feedbackSlice = createSlice({
  name: "feedbacks",
  initialState,
  reducers: {
    addFeedback: (state, action: PayloadAction<IFeedback>) => {
      state.feedbacks?.push(action.payload);
    },
    deleteFeedback: (state, action: PayloadAction<{ id: number }>) => {
      console.log("Come delete");
      state.feedbacks = state.feedbacks?.filter(
        (item) => item.id !== action.payload.id
      );
    },
    updateFeedback: (state, action: PayloadAction<IFeedback>) => {
      console.log("Come here update", action.payload);
      const { id } = action.payload;
      state.feedbacks = state.feedbacks?.map((feedback) =>
        feedback.id === id ? action.payload : feedback
      );
      state.feedbackEdit.edit = false;
      state.feedbackEdit.item = null;
    },
    setEdit: (state, action: PayloadAction<IFeedback>) => {
      state.feedbackEdit.edit = true;
      state.feedbackEdit.item = action.payload;
    },
  },
});

export const { addFeedback, deleteFeedback, setEdit, updateFeedback } =
  feedbackSlice.actions;

export default feedbackSlice.reducer;
